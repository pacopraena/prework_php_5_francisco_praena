<?php

function securitySave($fichero){
    $nombreDir = date("Y-m-d h.i");
    $nuevoFichero = $nombreDir.'/'.$fichero.'.modificado';

    mkdir($nombreDir, 0777);

    copy($fichero, $nuevoFichero);

}

securitySave('documento.doc');